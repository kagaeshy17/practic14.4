#include <iostream>
#include <stdint.h>
#include <string>
#include <iomanip>

int main()
{
	std::string name = "JerbosPork";
	std::cout << "Word = " << name << "\n";

	std::cout << "String leight = " << name.length() << "\n";

	char firstChar = name.at(0);
	std::cout << "First character = " << firstChar << "\n";

	char lastChar = name.at(name.length() - 1);
	std::cout << "Last character = " << lastChar << "\n";

}